/**
 * \file audio.h
 * \brief Header file for audio functionality
 * \author Rhys Kessey
*/

#ifndef AUDIO_H_
#define AUDIO_H_





#ifdef __cplusplus
extern "C" {
#endif

#include "types.h"
#include "timer.h"
#include "inout.h"
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <wiringPi.h>


#ifdef __cplusplus
}
#endif




void speaker_init(void);
int AUBeep(void);                                   // Play beep sound
int AUTone(int freq, int time);                     // Play tone with FREQ for time/1000 sec
int AUCheckTone(void);                              // Non-blocking test if tone has finished
int AUPlay(BYTE* sample);                           // Play audio sample
int AUCheckPlay(void);                              // Non-blocking test if playing has finished
int AUMicrophone(void);                             // Return microphone A-to-D sample value
int AURecord(BYTE* buf, int time, long freq);       // Record sound for time/1000sec using sample FREQ
int AUCheckRecord(void);                            // Non-blocking check whether recording has finished

#endif /* AUDIO_H_ */
