/**
 *  \file camera.h
 *  \brief Camera routines for the Raspberry Pi 2, modded for the EyeBot.
 *  \author Jeremy Tan & Marcus Pham
 */

#ifndef _CAMERA_H
#define _CAMERA_H

/** Success code **/
#define CAM_SUCCESS 1
/** Failure code **/
#define CAM_FAILURE 0



#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <opencv/cv.h>
#include <opencv2/highgui/highgui_c.h>
#include "types.h"

#ifdef __cplusplus
}
#endif



int CAMSIZE;
int CAM_INITIAL;

int CAMInit();

int CAMSetMode(int mode);

int CAMGetMode ();

int CAMGet (BYTE *buf);

extern int CAMRelease (void);

int CAMGetGray(BYTE *buf);

#endif
