/**
 *   Image Processing Library
 *   Thomas Braunl, Univ. Stuttgart, 1996
 *   Additional Code by Gerrit Heitsch
 *   Adapted by Michael Rudolph
 *   Adapted for RoBiOS by Thomas Lampart
 *   derived after Braunl et al. : Parallele Bildverarbeitung, Addison-Wesley
 *
 *   $Id: improc.h,v 1.1.1.1 2001/08/23 06:29:38 braunl Exp $
 * \file imageProc.h
 * \brief defines image processing fucntions
 * \author Thomas Braunl
 * \author Ke Feng 
 * \author by Marcus Pham
 */

#ifndef IMAGEPROC_H_
#define IMAGEPROC_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#ifdef __cplusplus
}
#endif



int IP_CAMWIDTH, IP_CAMHEIGHT, IP_CAMSIZE;

/** Success code **/
#define IP_SUCCESS 1
/** Failure code **/
#define IP_FAILURE 0

#define MAX_IMAGE_SIZE (820*620)
#define MAX_CONTOUR  100000

/**
 * \brief Structure defining the IP configs
 */
typedef struct
{
    int colorBlack;
    int colorWhite;
} IPL_CONFIG;

/**
 * \brief Structure defining an IP point
 */
typedef struct
{
    short x;
    short y;
} IPL_POINT;

/**
 * \brief Structure defining a IP distance
 */
typedef struct
{
    double x;
    double y;
} XYDISTANCE;
    
    int IPInitialised;

    int IPSetSize(int mode);
    
    int IPReadFile(char *filename, BYTE* img);       // Read PNM file, fill/crop if req.; return 0 for color, 1 for gray image
    int IPWriteFile(char *filename, BYTE* img);      // Write color PNM file
    int IPWriteFileGray(char *filename, BYTE* gray); // Write gray scale PGM file
    int IPLaplace(BYTE* grayIn, BYTE* grayOut);      // Laplace edge detection on gray image
    int IPSobel(BYTE* grayIn, BYTE* grayOut);        // Sobel edge detection on gray image
    int IPCol2Gray(BYTE* imgIn, BYTE* grayOut);      // Transfer color to gray image
    int IPGray2Col(BYTE* imgIn, BYTE* colOut);       // Transfer gray to color image
    
    //missing!!!
    COLOR IPRGB2ColPixel(BYTE R, BYTE G, BYTE B);    // RGB to color for pixel
    int IPRGB2Col(BYTE* r, BYTE* g, BYTE* b, BYTE* imgOut); // Transform 3*gray to color image
    
    //missing!!!
    int IPCol2HSIPixel(COLOR c, BYTE h, BYTE s, BYTE i);    // RGB to HSI for pixel
    
    int IPCol2HSI(BYTE* img, float* h, float* s, float* i);    // Transform RGB image to HSI image
    
    int IPOverlay(BYTE* c1, BYTE* c2, BYTE* cOut);          // Overlay c2 onto c1, all color images
    int IPOverlayGray(BYTE* g1, BYTE* g2, int rgb, BYTE* cOut); // Overlay gray image g2 onto g1, rgb (0=red, 1=green, 2=blue), cOut in color
    
    
    
int Centroid(BYTE* Nucleus);                    //calculate the gravity center of the usan.
double Susan1(int t,BYTE* Nucleus);             //Mask for susan corner detector.
int NonMaximum(BYTE* imagein,BYTE* imageout);  //Mask for NonMaximum suppression.
int Susan(int t,BYTE* imagein,BYTE* MarkCorner);//Susan corner detector;
//t:the threshold between the value of pixels,general real image ------->t=25,low contrast image------->t=7;
void HornSchunk(BYTE* grayimage1,BYTE* grayimage2,int alpha2,int iters,BYTE* imageout);
float laplace2(float * td);
void  IPmean(BYTE* grayIn,BYTE* grayOut);
void laplace_envir(float* imagein,float* imageout);
void Sobelx(BYTE* grayIn,BYTE* grayOut);
void Sobely(BYTE* grayIn,BYTE* grayOut);
void IPLOverLay(BYTE* imagein1,BYTE* imagein2,BYTE* imageout);		//overlay two color images .



/*
 *    Die global sichtbaren Funktionen der Image Processing Library
 *    Sie beginnen alle mit IPL_, um name clashes zu vermeiden.
 */

void IPL_init(IPL_CONFIG * config);
void IPL_laplace(BYTE * imageIn, BYTE * imageOut);
void IPL_sobel(BYTE * imageIn, BYTE * imageOut);
void IPL_mean(BYTE * imageIn, BYTE * imageOut);
void IPL_threshold(BYTE * imageIn, BYTE * imageOut, BYTE threshold);
void IPL_gray_stretch(BYTE * imageIn, BYTE * imageOut);
void IPL_gray_reduce(BYTE * imageIn, BYTE * imageOut, int numvals);
void IPL_gen_histogram (BYTE * imageIn, int *histogram);
void IPL_equal_histogram (BYTE * imageIn, BYTE * imageOut, int *histogram);
void IPL_erosion (BYTE * imageIn, BYTE * imageOut, char * struc);
void IPL_dilation (BYTE * imageIn, BYTE * imageOut, char * struc);
void IPL_open (BYTE * imageIn, BYTE * imageOut, char * struc);
void IPL_close (BYTE * imageIn, BYTE * imageOut, char * struc);
void IPL_fill (BYTE * imageIn, BYTE * imageOut, int x, int y, char * struc);
void IPL_connected(BYTE * imageIn, BYTE * imageOut, int x, int y, char * struc);
void IPL_boundary(BYTE * imageIn, BYTE * imageOut, char * struc);
void IPL_skeleton(BYTE * imageIn, BYTE * imageOut, char * struc);
void IPL_identity(BYTE * imageIn, BYTE * imageOut);
int  IPL_contour(BYTE * imageIn, BYTE * imageOut, IPL_POINT * result, BYTE threshold);
void IPL_median(BYTE * imageIn, BYTE * imageOut);
void IPL_min(BYTE * imageIn, BYTE * imageOut);
void IPL_max(BYTE * imageIn, BYTE * imageOut);
void IPL_negation(BYTE * imageIn, BYTE * imageOut);
void IPL_difference(BYTE * image1, BYTE * image2, BYTE * imageOut);
void IPL_noise(BYTE * imageIn, BYTE * imageOut, double noise);
int  IPL_count(BYTE * imageIn, BYTE * imageOut, BYTE val);
int  IPL_count_nobound(BYTE * imageIn, BYTE * imageOut, BYTE val);
void IPL_corner(BYTE * imageIn, BYTE * imageOut);
void IPL_and(BYTE *imageIn1, BYTE * imageIn2, BYTE *imageOut);
void IPL_or(BYTE *imageIn1, BYTE * imageIn2, BYTE *imageOut);
int  IPL_equal(BYTE *imageIn1, BYTE *imageIn2);
int  IPL_equal_nobound(BYTE *image1, BYTE *image2);
void IPL_showxy(BYTE *imageIn, BYTE *imageOut, int x, int y);
void IPL_dither(BYTE *imageIn, BYTE *imageOut);
void IPL_overlay(BYTE *imageIn1, BYTE *imageIn2, BYTE *imageOut, BYTE gray_val);
void IPL_region_growing(BYTE *imageIn, BYTE *imageOut, int thresh);
int  IPL_FindCircles(BYTE * imageIn, BYTE * imageOut, XYDISTANCE * result);

#endif
