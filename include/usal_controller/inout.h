/**
 * \file   inout.h
 * \brief  Defines all the functions to connect to a serial USB/serial connection
 * \author Marcus Pham
 */

#ifndef INOUT_H_
#define	INOUT_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "serial.h"
#ifdef __cplusplus
}
#endif



int DIGITALInitialised;

int DIGITALInit(void);                          // starts the serial
int DIGITALSetup(int io, char direction);       // Set IO line [1..16] to i-n/o-ut/I-n pull-up/J-n pull-down
int DIGITALRead(int io);                        // Read and return individual input line [1..16]
int DIGITALReadAll(void);                       // Read and return all 16 io lines
int DIGITALWrite(int io, int state);            // Write individual output [1..16] to 0 or 1
int ANALOGRead(int channel);                    // Read analog channel [1..8]
int ANALOGVoltage(void);                        // Read analog supply voltage in [Volt/100]
int IOBoardVer(char* version);

#endif /* INOUT_H */
