
/**
 * \file    lcd.h
 * \brief   Header file for the LCD functions
 * \author  Marcus Pham & Remi KEAT 
 * Using some functions by George Peter Staplin 2003 GeorgePS@XMission.com
 */

#ifndef LCD_H_
#define LCD_H_

#define _GNU_SOURCE

#ifdef __cplusplus
extern "C" {
#endif
#include <stdarg.h>
#include "types.h"
#include "system.h"
#include "globals.h"

#include "key.h"
#include "listmenu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <jpeglib.h>
#include <jerror.h>

#include <X11/Xutil.h>
#include <X11/Xresource.h>
#include <X11/keysym.h>
#include <math.h>
#ifdef __cplusplus
}
#endif


//defining the Font Names
#define HELVETICA 0
#define TIMES 1
#define COURIER 2

//defining the font variations
#define NORMAL 0
#define BOLD 1
#define ITALICS 2

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define LCD_MAX_LIST_ITEM MAX(1,(gLCDHandle->height/(gLCDHandle->fontHeight*2))-2)

//defining the colours
#define LCD_WHITE getColor("white")
#define LCD_SILVER getColor("light gray")
#define LCD_LIGHTGRAY getColor("light gray")
#define LCD_LIGHTGREY getColor("light grey")
#define LCD_GRAY getColor("gray")
#define LCD_DARKGRAY getColor("dark gray")
#define LCD_DARKGREY getColor("dark grey")
#define LCD_BLACK getColor("black")
#define LCD_BLUE getColor("blue")
#define LCD_NAVY getColor("navy")
#define LCD_AQUA getColor("aquamarine")
#define LCD_CYAN getColor("cyan")
#define LCD_TEAL getColor("dark cyan")
#define LCD_FUCHSIA getColor("magenta")
#define LCD_MAGENTA getColor("magenta")
#define LCD_PURPLE getColor("purple")
#define LCD_RED getColor("red")
#define LCD_MAROON getColor("maroon")
#define LCD_YELLOW getColor("yellow")
#define LCD_OLIVE getColor("dark olive green")
#define LCD_LIME getColor("lime green")
#define LCD_GREEN getColor("green")

//defining RGB Colours
#define RED 0xFF00000
#define GREEN 0x00FF00
#define BLUE 0x0000FF
#define WHITE 0xFFFFFF
#define GRAY 0x808080
#define BLACK 0x000000
#define SILVER 0xC0C0C0
#define LIGHTGRAY 0xD3D3D3
#define DARKGRAY 0xA9A9A9
#define NAVY 0x000080
#define CYAN 0x00EEEE
#define TEAL 0x008080
#define MAGENTA 0xFF00FF
#define PURPLE 0x800080
#define MAROON 0x800000
#define YELLOW 0xFFFF00
#define OLIVE 0x9ACD32
#define ORANGE 0xFFA500

/* constants for text colorflags */
#define LCD_BGCOL_TRANSPARENT         0x01
#define LCD_BGCOL_NOTRANSPARENT       0x10

#define LCD_BGCOL_INVERSE             0x02
#define LCD_BGCOL_NOINVERSE           0x20

#define LCD_FGCOL_INVERSE             0x04
#define LCD_FGCOL_NOINVERSE           0x40

/* constants for lcd modes */
#define LCD_AUTOREFRESH               0x0001
#define LCD_NOAUTOREFRESH             0x0100

#define LCD_SCROLLING                 0x0002
#define LCD_NOSCROLLING               0x0200

#define LCD_LINEFEED                  0x0004
#define LCD_NOLINEFEED                0x0400

#define LCD_SHOWMENU                  0x0008
#define LCD_HIDEMENU                  0x0800

#define LCD_LISTMENU				  0x0010
#define LCD_CLASSICMENU				  0x1000

#define LCD_FB_ROTATE                 0x0080
#define LCD_FB_NOROTATION             0x8000

/*
 * define fonts using their alias
 * font aliases can be found in the following file:
 * 	/etc/X11/fonts/misc/xfonts-base.alias
 */
#define FONTDEFAULT         "6x13"
#define FONTBOLD            "6x13bold"

//for Rpi2: 7x14

int LCD_INITIAL;
int IM_TYPE;
int IM_LEFT;
int IM_TOP;
int IM_WIDTH;
int IM_LENGTH;

int LCDDrawFrame(int x1, int y1, int x2, int y2, XColor color);
int LCDDrawMenu(void);
int LCDDrawList(void);

int LCDInit();
int LCDClear(void);

int LCDSetMode(HWORD mode);
// Set LCD Mode (0=default, XXX)

HWORD LCDGetMode(void);
int LCDResetMode(HWORD mode);
int LCDMenu(char *string1, char *string2, char *string3, char *string4);


int LCDMenuIRaw(int pos, char *string, XColor fgcol, XColor bgcol, void* userp);
int LCDMenuI(int pos, char *string, COLOR fg, COLOR bg);

MENU_ITEM* LCDMenuItem(int index);

int LCDList(LIST_MENU *menulist);
int LCDSetList(LIST_MENU* menulist);
LIST_MENU* LCDGetList(void);
RECT* LCDListBox(int pos);
MENU_ITEM* LCDListActiveItem(void);

XColor getColor(char* colorName);
XColor InvertColor(XColor color);

int LCDNeedRefresh(void);

//same as LCDRect?
int LCDAreaRaw(int x1, int y1, int x2, int y2, XColor color);
int LCDArea(int x1, int y1, int x2, int y2, COLOR col, int fill);

int LCDSetPixel(int x, int y, XColor color);
int LCDPixel(int x, int y, COLOR col);

XColor LCDGetPixel(int x, int y);
int LCDPixelInvert(int x, int y);

int LCDLineRaw(int x1, int y1, int x2, int y2, XColor color);
int LCDLine(int x1, int y1, int x2, int y2, COLOR col);

int LCDLineInvert(int x1, int y1, int x2, int y2);
int LCDAreaInvert(int x1, int y1, int x2, int y2);

int LCDCircle(int x1, int y1, int size, COLOR col, int fill);     // Draw filled/hollow circle
int LCDCircleInvert(int x1, int y1, int size);           // Invert circle

int LCDFrame(int x1, int y1, int x2, int y2, XColor color);
int LCDRect(int x1, int y1, int x2, int y2, COLOR col);

int LCDTextColor(XColor fgcol, XColor bgcol, char colorflags);
int LCDSetColor(COLOR fg, COLOR bg);

int LCDPrintf(const char *format, ...);
int LCDPrintfFont(EYEFONT eyeFont,const char *format, ...);
int LCDSetPrintf(int row, int column, const char *format, ...);
int LCDSetPrintfFont(int row, int column, EYEFONT eyeFont, const char *format, ...);

int LCDSetPos(int row, int column);
int LCDGetPos(int *row, int *column);

RECT LCDTextBar(int row, int column, int length, int fill, XColor color);

int LCDRelease();
int LCDRefresh(void);
int LCDRefresh2(void);
int LCDRefresh3(void);
int LCDGetFBInfo(FBINFO* pinfo);
int LCDListCount(void);
int LCDListIndex(int index);
int LCDListScrollUp(void);
int LCDListScrollDown(void);

//check up on these
int LCDPutImageRGB(int xpos, int ypos, int xsize, int ysize, BYTE* data);
int LCDPutImageGrey(int xpos, int ypos, int xsize, int ysize, BYTE* data);

int LCDPutImageJPG(int xpos, int ypos, int xsize, int ysize, char *path);

int LCDImageStart(int t, int x, int y, int xs, int ys);  // Define image type, start position and size (default QQVGA; 0,0; full)
int LCDImage(BYTE *img);                                 // Print color image at screen start pos. and size
int LCDImageGray(BYTE *g);                               // Print gray image

int LCDSetFontRaw(char *fontAlias);
int LCDSetFont(int font, int variation);
int LCDSetFontSize(int fontsize);

int LCDGetSize(int* x, int* y);                                  // Get LCD resolution in pixels

#endif /* LCD_H_ */
