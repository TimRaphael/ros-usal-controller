/**
 * \file    listmenu.h
 * \brief   creates lists
 * \author  Marcus Pham (Remi Keat)
 */

#ifndef LISTMENU_H_
#define LISTMENU_H_



#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#include <stdlib.h>
#ifdef __cplusplus
}
#endif
 


void listmenuInit(LIST_MENU *,char *);
void listmenuAddItem(LIST_MENU *,char *,void *);
void listmenuFreeItems(LIST_MENU *);
int listmenuSize(LIST_MENU);
void listmenuToggleScroll(LIST_MENU *);
int listmenuGetScroll(LIST_MENU);
void listmenuChangeItemText(LIST_MENU *listmenu, char *newText,int index);
int listmenuGetIndex(LIST_MENU listmenu);
char *listmenuGetItemText(LIST_MENU listmenu);

#endif
