/**
 * \file    psd_sensors.h
 * \brief   Header file for the PSD sensors functions
 * \author  Marcus Pham
 */

#ifndef PSD_SENSORS_H_
#define PSD_SENSORS_H_

#ifdef __cplusplus
extern "C" {
#endif
#include "serial.h"
#include "types.h"
#ifdef __cplusplus
}
#endif




HDT_PSD *PSDFirst;

int PSDInitialised;

int PSDInit();
int PSDGetRaw(int psd);
int PSDGet(int psd);

#endif /* PSD_SENSORS_H_ */
