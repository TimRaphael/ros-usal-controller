/**
 * \file   radio.h
 * \brief  Defines all the functions to connect to multiple EyeBots
 * \author Marcus Pham
 */


int RADIOInit(void);                            // Start radio communication
int RADIOGetID(void);                           // Get own radio ID
int RADIOSend(int id, BYTE* buf, int size);     // Send spec. number of bytes to ID destination
int RADIOReceive(int *id, BYTE* buf, int *size);// Read num. of bytes from ID source
int RADIOCheck(int *id);                        // Non-blocking check whether message is waiting
int RADIOStatus(void);                          // Returns number of robots (incl. self) in network
int RADIORelease(void);                         // Terminate radio communication