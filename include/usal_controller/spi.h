/**
 * \file    spi.h
 * \brief   Header file for the SPI functions
 * \author  Remi KEAT
 */

#ifndef SPI_H_
#define SPI_H_



#ifdef __cplusplus
extern "C" {
#endif
#include "spi_commands.h"
#include "globals.h"
#include "hdt.h"
#include <unistd.h>
#include <mpsse.h>
#ifdef __cplusplus
}
#endif



typedef struct mpsse_context* SPI_HANDLE;

SPI_HANDLE SPIInit(int deviceNumber);
int SPIRelease(SPI_HANDLE spiHandle);
int SPISend(SPI_HANDLE spiHandle, size_t length, const uint8_t data[]);
int SPISendDefault(size_t length, const uint8_t data[]);
int SPIRead(SPI_HANDLE spiHandle, size_t length, uint8_t* data[]);
int SPIReadDefault(size_t length, uint8_t* data[]);

#endif /* SPI_H_ */
