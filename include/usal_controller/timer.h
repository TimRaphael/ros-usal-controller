/**
 * \file    timer.h
 * \brief	Header file for timer functionality
 * \author	Rhys Kessey
*/

#ifndef TIMER_H_
#define TIMER_H_



#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"
#include <sys/time.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <wiringPi.h>
#ifdef __cplusplus
}
#endif

 


typedef int Timer;

int OSWait(int n);                                    	// Wait for n/1000 sec
Timer OSAttachTimer(int scale, void (*fct)(void));   	// Add fct to 1000Hz/scale timer
int   OSDetachTimer(Timer handle);                      // Remove fct from 1000Hz/scale timer
int OSSetTime(int hrs,int mins,int secs);               // Set system time
int OSGetTime(int *hrs,int *mins,int *secs,int *ticks); // Get system time (ticks in 1/1000 sec)
int OSShowTime(void);                                   // Print time on LCD
int OSGetCount(void);                                   // Count in 1/1000 sec since system start


#endif /* TIMER_H_ */
