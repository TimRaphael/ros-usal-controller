/**
 * \file		types.h
 * \brief		Defines types used for the EyeBot 7
 * \author      Remi Keat & MArcus Pham
 */

#ifndef TYPES_H_
#define TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <X11/Xlib.h>
#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#ifdef __cplusplus
}
#endif



#define VEHICLE 1
#define PLATFORM 2
#define WALKER 3

#define DEBUG 1
//#define MIN(a,b) (((a)<(b))?(a):(b))
//#define MAX(a,b) (((a)>(b))?(a):(b))

#define NUMBER_TRY 10
#define HDT_MAX_NAMECHAR 80
#define LCD_MENU_STRLENGTH 32 /* for storage declaration */
#define LCD_LIST_STRLENGTH 64 /* for storage declaration */
#define MENU_HEIGHT 38
#define KEYTM_MAX_REGIONS 32
#define VERSION "1.0"
#define MACHINE_SPEED 700000000
#define MACHINE_TYPE VEHICLE
#define MACHINE_NAME "EyeBot"
#define ID 1
#define LIBM6OS_VERSION "1.0"

#define HDT_FILE "/home/pi/eyebot/bin/hdt.txt"

#define HDT_MAX_PATHCHAR 256
#define HDT_MAX_FILECHAR 40
#define HDT_MAX_READBUFF 128

#define IOBOARD 1

typedef char* DeviceSemantics;

//may need to change to int
typedef unsigned short HWORD;
typedef unsigned long KEYCODE;
typedef unsigned char KEYMODE;
typedef unsigned short LCDMODE;

typedef int COLOR;
typedef char BYTE;

int CAMWIDTH;
int CAMHEIGHT;
int CAMMODE;

//defining the image dimensions
#define QQVGA_X 160
#define QQVGA_Y 120
#define QQVGA_SIZE (QQVGA_X*QQVGA_Y*3)
#define QVGA_X 320
#define QVGA_Y 240
#define QVGA_SIZE (QVGA_X*QVGA_Y*3)
#define VGA_X 640
#define VGA_Y 480
#define VGA_SIZE (VGA_X*VGA_Y*3)

#define CAM1MP_X 1296
#define CAM1MP_Y 730
#define CAM1MP_SIZE (CAM1MP_X*CAM1MP_Y*3)
#define CAMHD_X 1920
#define CAMHD_Y 1080
#define CAMHD_SIZE (CAMHD_X*CAMHD_Y*3)
#define CAM5MP_X 2592
#define CAM5MP_Y 1944
#define CAM5MP_SIZE (CAM5MP_X*CAM5MP_Y*3)

//defining the image types
#define QQVGA 0
#define QVGA  1
#define VGA   2
#define CAM1MP 3
#define CAMHD 4
#define CAM5MP 5

//the font name length
#define FONTNAMELEN 50
 
typedef BYTE VGAcol[480][640][3];
typedef BYTE VGAgray[480][640] ;
typedef BYTE QVGAcol[240][320][3] ;
typedef BYTE QVGAgray[240][320];
typedef BYTE QQVGAcol[120][160][3];
typedef BYTE QQVGAgray[120][160];
typedef BYTE CAM1MPcol[730][1296][3];
typedef BYTE CAM1MPgray[730][1296];
typedef BYTE CAMHDcol[1080][1920][3];
typedef BYTE CAMHDgray[1080][1920];
typedef BYTE CAM5MPcol[1944][2592][3];
typedef BYTE CAM5MPgray[1944][2592];

/**
 * \brief Structure defining a Hints for the LCD
 */
typedef struct
{
	unsigned long flags;
	unsigned long functions;
	unsigned long decorations;
	long inputMode;
	unsigned long status;
} HINTS;

/**
 * \brief Structure defining a Screen for the LCD
 */
typedef struct
{
	int xres, yres;
	int bpp;
} SCREEN;

/**
 * \brief Structure defining the cursor for the LCD
 */
typedef struct
{
	int x, y;
	int xmax, ymax;
} CURSOR;

/**
 * \brief Structure defining the Framebuffer info for the LCD
 */
typedef struct
{
	SCREEN screen;
	CURSOR cursor; /* x & y holds font width & height! */
} FBINFO;

/**
 * \brief Structure defining the CPU info
 */
typedef struct
{
	char name[40];
	char mhz[20];
	char arch[20];
	char bogomips[20];
} INFO_CPU;

/**
 * \brief Structure defining the memory info
 */
typedef struct
{
	char procnum[20];
	char total[40];
	char free[40];
} INFO_MEM;

/**
 * \brief Structure defining the processor info
 */
typedef struct
{
	char num[20];
} INFO_PROC;

/**
 * \brief Structure defining battery info
 */
typedef struct
{
	char uptime[20];
	char vbatt[20];
	int vbatt_8;
} INFO_MISC;

/**
 * \brief Structure representing the coordinates of a point
 */
typedef struct
{
	int x, y;
} COORD_PAIR;

/**
 * \brief Structure defining a rectangular box on the LCD
 */
typedef struct
{
	int active;
	COORD_PAIR tl;       //top left
	COORD_PAIR br;       //bottom right
} BOX;

/**
 * \brief Structure defining boxes for touchscreen use
 */
typedef struct
{
	KEYMODE mode;
	BOX rect[KEYTM_MAX_REGIONS];
} TOUCH_MAP;

/**
 * \brief Structure defining touchscreen presses
 */
typedef struct
{
	COORD_PAIR point, value;
	int sync, status;
} TOUCH_EVENT;

/**
 * \brief Structure defining a menu item
 */
typedef struct
{
	char label[LCD_MENU_STRLENGTH];
	XColor fgcol, bgcol;
	void *plink; /* link to user data/function! */
} MENU_ITEM;

/**
 * \brief Structure defining a menu
 */
typedef struct
{
	char title[LCD_LIST_STRLENGTH];
	XColor fgcol, bgcol;
	int size, start, width, left, scroll; /* configure these! */
	int index/*, count*/; /* the library will set & manage these! */
	MENU_ITEM *pitems; /* pointer to array of 'size' menuitems */
	int no_empty;
} LIST_MENU;

/**
 * \brief Structure defining an LCD
 */
typedef struct
{
	int lcdNum;
	Display* d;
	int s;
	Window w;
	Colormap colormap;
	GC gc;
	XFontStruct* fontstruct;
    int fontType;
    int fontVariation;
    int fontSize;
	int fontHeight;
	int fontWidth;
	int height;
	int width;
	int startCurPosX;
	int startCurPosY;
	XColor fgTextColor;
	XColor bgTextColor;
	char colorflag;
	HWORD mode;
	MENU_ITEM menuItems[4];
	LIST_MENU* listMenu;
	int fd;
	bool X11Error;
	Screen *scr;
} LCD_HANDLE;

/**
 * \brief Structure representing a rectangle
 */
typedef struct
{
	int x, y;
	int height, width;
} RECT;

/**
 * \brief Structure defining a HDT entry
 */
typedef struct HDT_ENTRY
{
	int length;
	char *buffer;
}
HDT_ENTRY;

/**
 * \brief Structure defining a HDT Table
 */
typedef struct HDT_TABLE
{
	struct HDT_TABLE *pnext;
	char name[HDT_MAX_NAMECHAR];
	int size;
	int *data;
}
HDT_TABLE;

/**
 * \brief Structure defining a HDT Device
 */
typedef struct HDT_DEVICE
{
	struct HDT_DEVICE *pnext;
	char name[HDT_MAX_NAMECHAR];
	char tabname[HDT_MAX_NAMECHAR];
	HDT_TABLE *ptable; /* pointer to a table */
}
HDT_DEVICE;

/**
 * \brief Structure defining a HDT Camera
 */
typedef struct HDT_CAM
{
	struct HDT_CAM *pnext;
	char name[HDT_MAX_NAMECHAR];
	int regaddr, ucb1400io, width, height;
}
HDT_CAM;

/**
 * \brief Structure defining a HDT Motor
 */
typedef struct HDT_MOTOR
{
	struct HDT_MOTOR *pnext;
	char name[HDT_MAX_NAMECHAR];
	char tabname[HDT_MAX_NAMECHAR];
	HDT_TABLE *ptable; /* pointer to a table */
    int regaddr;
}
HDT_MOTOR;

/**
 * \brief Structure defining a HDT encoder
 */
typedef struct HDT_ENCODER
{
	struct HDT_ENCODER *pnext;
	char name[HDT_MAX_NAMECHAR];
	char motorname[HDT_MAX_NAMECHAR];
	HDT_MOTOR *pmotor;
	int regaddr, clickspm;
	int maxspeed;
}
HDT_ENCODER;

/**
 * \brief Structure defining a HDT servo
 */
typedef struct HDT_SERVO
{
	struct HDT_SERVO *pnext;
	char name[HDT_MAX_NAMECHAR];
    char tabname[HDT_MAX_NAMECHAR];
    HDT_TABLE *ptable; /* pointer to a table */
	int regaddr, freq;
	int mintime, maxtime;
    int low, high;
}
HDT_SERVO;

/**
 * \brief Structure defining a HDT psd
 */
typedef struct HDT_PSD
{
	struct HDT_PSD *pnext;
	char name[HDT_MAX_NAMECHAR];
	char tabname[HDT_MAX_NAMECHAR];
	HDT_TABLE *ptable; /* pointer to a table */
	int regaddr;
}
HDT_PSD;

/**
 * \brief Structure defining a HDT drive
 */
typedef struct HDT_DRIVE
{
    struct HDT_DRIVE *pnext;
    char name[HDT_MAX_NAMECHAR];
    char encname1[HDT_MAX_NAMECHAR],encname2[HDT_MAX_NAMECHAR];
    char encname3[HDT_MAX_NAMECHAR],encname4[HDT_MAX_NAMECHAR];
    HDT_ENCODER *penc1, *penc2, *penc3, *penc4;
    int drivetype;
    int wheeldist1, axesdist, wheeldist2;
}
HDT_DRIVE;

/**
 * \brief Structure defining a HDT IRTV
 */
typedef struct HDT_IRTV
{
	struct HDT_IRTV *pnext;
	char name[HDT_MAX_NAMECHAR];
	int type, length, togmask, invmask, mode, buffsize, delay;
}
HDT_IRTV;

/**
 * \brief Structure defining a HDT ADC
 */
typedef struct HDT_ADC
{
	struct HDT_ADC *pnext;
	char name[HDT_MAX_NAMECHAR];
	char procname[HDT_MAX_NAMECHAR];
	int denom;
}
HDT_ADC;

/**
 * \brief Structure defining a HDT COM
 */
typedef struct HDT_COM
{
	struct HDT_COM *pnext;
	char name[HDT_MAX_NAMECHAR];
	char devname[HDT_MAX_NAMECHAR];
}
HDT_COM;

/**
 * \brief Structure defining a LCD fontcolour/name
 */
typedef struct 
{
	XColor fontColour;
	char fontName[FONTNAMELEN];
} EYEFONT;
#endif /* TYPES_H_ */
