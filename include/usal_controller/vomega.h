/**
 * \file		vomega.h
 * \brief		Header file for the VW functions
 * \author      Marcus Pham
 */

#ifndef VOMEGA_H_
#define VOMEGA_H_



#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#include "serial.h"
#include "types.h"
#ifdef __cplusplus
}
#endif
 


int VWInitialised;

int VWInit();

int VWSetSpeed(int linSpeed, int angSpeed);    // Set fixed linSpeed (mm/s) and angSpeed [(rad/100)/s]
int VWGetSpeed(int *linSspeed, int *angSpeed); // Read current speeds
int VWSetPosition(int x, int y, int phi);      // Set robot position to x, y [mm], phi [rad/100]
int VWGetPosition(int *x, int *y, int *phi);   // Get robot position as x, y [mm], phi [rad/100]
int VWControl(int Vv, int Tv, int Vw, int Tw); // Set PI params. for v and w (typical 70 30 70 10)
int VWControlOff(void);                        // Stop PI control for v and w
int VWStraight(int dist, int lin_speed);        // Drive straight, dist [mm], lin. speed [mm/s]
int VWTurn(int angle, int ang_speed);           // Turn on spot, angle [rad/100], ang. speed [(rad/100)/s]
int VWCurve(int dist, int angle, int lin_speed);// Drive Curve, dist [mm], angle [rad/100], lin. speed [mm/s]
int VWDriveRemain(void);                        // Return remaining drive time in 1/100 sec
int VWDriveDone(void);                          // Non-blocking check whether drive is finished (1) or not (0)
int VWDriveWait(void);                          // Suspend current thread until drive operation has finished
int VWStalled(void);                            // Returns number of stalled motor [1..2], 3 if both stalled, 0 if none

#endif /* VOMEGA_H_ */
