#include "ros/ros.h"
#include "std_msgs/String.h"
#include <unistd.h>


#ifdef __cplusplus
extern "C" {
#endif
#include "usal_controller/servos_and_motors.h"
#include "usal_controller/psd_sensors.h"
#ifdef __cplusplus
}
#endif

std::map<std::string, int> dirCmds;


int thrusterSpeed = 0;
int depthSpeed = 0;
int rudderPosition = 128;

// 0 = centre
// 1 = left
// 2 = right
int lastDirection = 0;

const int RUDDER_MAX_POS = 255;
const int RUDDER_MIN_POS = 0;
const int RUDDER_TICK = 10;

const int MOTOR_SPEED_TICK = 5;
const int MOTOR_SPEED_MAX = 100;
const int MOTOR_SPEED_MIN = 0;

const int THRUSTER_MOTOR_NUMBER = 2;
const int DEPTH_MOTOR_NUMBER = 1;
const int RUDDER_SERVO_NUMBER = 1;

void setMotorSpeed(int motor, int speed)
{
	MOTORDriveRaw(motor,speed);
	ROS_INFO("Setting Motor %d: %d%%", motor, speed);
}

void setServoPosition(int servo, int position)
{
	SERVOSet(servo, position);
	SERVOSet(servo, position-10); //for reasons, don't ask.

	ROS_INFO("Setting Servo %d: %d", servo, position);
}


void messageCallback(const std_msgs::String::ConstPtr& msg)
{
	ROS_INFO("Recieved Cmd: %s", msg->data.c_str());

	int keyCount = dirCmds.count(msg->data.c_str());

	if(keyCount < 1)
	{
		std::cout << "Unregognised Key: " << msg->data.c_str();
	}
	else
	{
		int commandInt = dirCmds[msg->data.c_str()];

		switch(commandInt)
		{
			case 1:
				MOTORDriveRaw(THRUSTER_MOTOR_NUMBER,80);
				ROS_INFO("Moving Forward");

			break;

			case 2:
				MOTORDriveRaw(THRUSTER_MOTOR_NUMBER,0);
				ROS_INFO("Stopping");
				
			break;

			case 3:
				MOTORDriveRaw(DEPTH_MOTOR_NUMBER,80);
				ROS_INFO("Diving");
				usleep(5000000);
				MOTORDriveRaw(DEPTH_MOTOR_NUMBER,0);
				ROS_INFO("Surfacing");
				
			break;

			case 4:
				ROS_INFO("Rudder Left");
				SERVOSet(RUDDER_SERVO_NUMBER, 40);
				usleep(2000000);
				SERVOSet(RUDDER_SERVO_NUMBER, 50);
				lastDirection = 1;
				
			break;

			case 5:
				ROS_INFO("Rudder Right");
				SERVOSet(RUDDER_SERVO_NUMBER, 240);
				usleep(2000000);
				SERVOSet(RUDDER_SERVO_NUMBER, 230);
				lastDirection = 2;
				
			break;

			case 6:
				ROS_INFO("Rudder Centre");
				if(lastDirection == 1)
				{
					SERVOSet(RUDDER_SERVO_NUMBER, 128);
					usleep(2000000);
					SERVOSet(RUDDER_SERVO_NUMBER, 120);
				}
				if(lastDirection == 2)
				{
					SERVOSet(RUDDER_SERVO_NUMBER, 140);
					usleep(2000000);
					SERVOSet(RUDDER_SERVO_NUMBER, 128);
				}

				lastDirection = 0;

			break;
		}
	}
}


int main(int argc, char** argv)
{
	ros::init(argc, argv, "usal_controller");

	ros::NodeHandle n;

	dirCmds["forward"] = 1;
	dirCmds["stop"] = 2;
	dirCmds["dive"] = 3;
	dirCmds["left"] = 4;
	dirCmds["right"] = 5;
	dirCmds["centre"] = 6;

	ros::Subscriber sub = n.subscribe("usal_dir_cmd", 1000, messageCallback);

	ros::spin();

	return 0;
}